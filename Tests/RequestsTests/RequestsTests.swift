import XCTest
import RxBlocking
@testable import Requests

final class RequestsTests: XCTestCase {

    func test_Get_WithRequestObject() {
        let request = TestGetRequest()
        let result = Requests.shared.make(request, mapTo: Users.self).firstBlocked
        XCTAssertNotNil(result)
    }
    
    func test_Get_WithArguments() {
        let result = Requests.shared.request(baseUrl: "https://api.github.com",
                                             path: "/users",
                                             mapTo: Users.self)
            .firstBlocked
        XCTAssertNotNil(result)
    }
        
    func test_Get_WithArguments_WithSharedBaseURL() {
        let requests = Requests.withBaseURL("https://api.github.com")
        let result = requests.request(path: "/users",
                                      mapTo: Users.self)
            .firstBlocked
        XCTAssertNotNil(result)
    }
        
    func test_Get_Dictionary() {
        let result = Requests.shared.make(baseUrl: "https://api.github.com",
                                          path: "/users")
            .firstBlocked
        XCTAssertNotNil(result)
    }
}
