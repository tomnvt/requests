import RxSwift

extension Single {

    var firstBlocked: Element? {
        return try! toBlocking().first()
    }

    var firstBlockedOptional: Element? {
        return try? toBlocking().first()
    }
}
