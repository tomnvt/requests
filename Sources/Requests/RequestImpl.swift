struct RequestImpl: Request {

    var baseURL: String?
    let path: String
    let method: Method
    let headers: [String: String]
    let params: Params

    init(baseURL: String,
         path: String,
         method: Method = .get,
         headers: [String: String] = [:],
         params: Params = .rawBodyText([:])) {
        self.baseURL = baseURL
        self.path = path
        self.method = method
        self.headers = headers
        self.params = params
    }
}
