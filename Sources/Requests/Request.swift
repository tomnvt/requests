import Foundation

public protocol Request {

    var baseURL: String? { get set }
    var path: String { get }
    var method: Method { get }
    var headers: [String: String] { get }
    var params: Params { get }
}

// MARK: - Default values
extension Request {

    var baseURL: String? { nil }
    var method: Method { .get }
    var headers: [String: String] { [:] }
    var params: Params { .rawBodyText([:]) }
    
    mutating func setBaseUrl(_ url: String?) {
        self.baseURL = url
    }
}

// MARK: - URL Request
extension Request {

    var urlRequest: URLRequest? {
        guard let baseURL = baseURL else { return nil }
        let url = URL(string: baseURL + path)!
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = headers
        request.httpMethod = method.rawValue
        switch params {
        case .rawBodyText(let params):
            let rawBody = params.reduce(into: "") { (result, keyValuePair) in
                result += "\(keyValuePair.key)=\(keyValuePair.value)&"
            }
            request.httpBody = rawBody.data(using: .utf8)
        case .rawBodyJson(let params):
            let postData = try? JSONSerialization.data(withJSONObject: params, options: [])
            request.httpBody = postData
        }
        return request
    }
}
