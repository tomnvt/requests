public enum Method: String {

    case get
    case post
    case put
    case delete
}
