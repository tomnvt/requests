import Foundation
import RxCocoa
import RxSwift

public struct Requests {

    let baseUrl: String?

    private var session = URLSession(configuration: URLSessionConfiguration.default)
}

// MARK: - Singleton
public extension Requests {

    static let shared = Self(baseUrl: "")
}

// MARK: - Init with base URL
public extension Requests {

    static func withBaseURL(_ url: String) -> Self {
        Self(baseUrl: url)
    }
}

// MARK: - Session configuration
public extension Requests {

    mutating func setSession(_ session: URLSession) {
        self.session = session
    }
}

// MARK: - Core
public extension Requests {

    func make<T: Codable>(_ request: Request, mapTo type: T.Type) -> Single<T> {
        return make(request: request)
            .map { try JSONDecoder().decode(T.self, from: $0) }
    }

    func make<T: Codable>(_ request: Request, mapTo type: [T].Type) -> Single<[T]> {
        return make(request: request)
            .map { try JSONDecoder().decode(type.self, from: $0) }
    }

    func request<T: Codable>(baseUrl: String? = nil,
                             path: String,
                             method: Method = .get,
                             headers: [String: String] = [:],
                             params: Params = .rawBodyText([:]),
                             mapTo type: [T].Type) -> Single<[T]> {
        guard baseUrl != nil || self.baseUrl != nil else {
            return .error(RequestsError.baseUrlMissing)
        }
        let request = RequestImpl(baseURL: baseUrl ?? self.baseUrl ?? "",
                                  path: path,
                                  method: method,
                                  headers: headers,
                                  params: params)
        return make(request: request)
            .map { try JSONDecoder().decode(type.self, from: $0) }
    }
    
    func make(request: Request) -> Single<Data> {
        var copy = request
        if copy.baseURL == nil {
            copy.setBaseUrl(self.baseUrl)
        }
        guard let urlRequest = copy.urlRequest else { return
            .error(RequestsError.gettingUrlFailedForRequest("\(request)")) }
        return session.rx.data(request: urlRequest)
            .asSingle()
            .observeOn(MainScheduler.asyncInstance)
            .subscribeOn(MainScheduler.asyncInstance)
    }

    func make(baseUrl: String? = nil,
              path: String,
              method: Method = .get,
              headers: [String: String] = [:],
              params: Params = .rawBodyText([:])) -> Single<[[String: Any]]> {
        guard baseUrl != nil || self.baseUrl != nil else {
            return .error(RequestsError.baseUrlMissing)
        }
        let request = RequestImpl(baseURL: baseUrl ?? self.baseUrl ?? "",
                                  path: path,
                                  method: method,
                                  headers: headers,
                                  params: params)
        return make(request: request)
            .map {
                try (JSONSerialization.jsonObject(with: $0, options: []) as? [[String: Any]] ?? [])
            }
    }
    
    func make(baseUrl: String? = nil,
              path: String,
              method: Method = .get,
              headers: [String: String] = [:],
              params: Params = .rawBodyText([:])) -> Single<[String: Any]> {
        guard baseUrl != nil || self.baseUrl != nil else {
            return .error(RequestsError.baseUrlMissing)
        }
        let request = RequestImpl(baseURL: baseUrl ?? self.baseUrl ?? "",
                                  path: path,
                                  method: method,
                                  headers: headers,
                                  params: params)
        return make(request: request)
            .map {
                try (JSONSerialization.jsonObject(with: $0, options: []) as? [String: Any] ?? [:])
            }
    }
}
