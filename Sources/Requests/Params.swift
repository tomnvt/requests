public enum Params {

    case rawBodyText([String: Any])
    case rawBodyJson([String: Any])
}
