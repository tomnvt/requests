public enum RequestsError: Error {

    case baseUrlMissing
    case gettingUrlFailedForRequest(String)
}
