// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "Requests",
    products: [
        .library(name: "Requests", targets: ["Requests"])
    ],
    dependencies: [
        .package(url: "https://github.com/ReactiveX/RxSwift.git", from: "5.0.0")
    ],
    targets: [
        .target(name: "Requests", dependencies: ["RxSwift", "RxCocoa", "RxBlocking"]),
        .testTarget(name: "RequestsTests", dependencies: ["Requests", "RxBlocking"])
    ]
)
